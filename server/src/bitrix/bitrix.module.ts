import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';

import { BitrixService } from './bitrix.service';

// Модуль реализует запросы к битриксу
@Module({
  imports: [
    HttpModule.register({
      baseURL: 'https://truecode.bitrix24.ru/rest/635/64ag306pv8a2fdff',
      timeout: 10000,
    }),
  ],
  providers: [BitrixService],
  exports: [BitrixService],
})
export class BitrixModule {}
